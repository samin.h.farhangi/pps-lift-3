seg=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/segments.txt  #E1:E15


for i in `cat $seg`
do
 echo $i
	awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' Pig_Liver_${i}_length | sed 's/chr//' > Pig_Liver_${i}_length_up.bed
 	awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' Pig_Spleen_${i}_length | sed 's/chr//' > Pig_Spleen_${i}_length_up.bed
	awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' Pig_Lung_${i}_length | sed 's/chr//' > Pig_Lung_${i}_length_up.bed
	awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' Pig_Muscle_${i}_length | sed 's/chr//' > Pig_Muscle_${i}_length_up.bed
done