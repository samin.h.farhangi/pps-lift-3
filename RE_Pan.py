#!/usr/bin/env python
from sys import argv

"""
@author: hosse004; 08-04-2022

This program parses a BED file of RE states obtained from Pan et al. 2021.
Input: BED file
Output: A tab-delimited file including: chromosome, start, stop, length, state, tissue
"""

# Function to parse the BED file and store the information in a dictionary
def parse_bed_file(bed_file, tissue):
    number = 0  
    bed_dict = {}  
    
    for line in bed_file:
        bed_line = line.strip().split()   
        chr = bed_line[0]   
        start = bed_line[1]  
        stop = bed_line[2]   
        state = bed_line[3]  
        length = str(int(stop) - int(start))   
        number += 1   
        bed_dict[number] = (chr, start, stop, length, state, tissue)   
    
    return bed_dict   
    
# Function to write the parsed information into separate output files based on state
def write_output(bed_dict, tissue, outname1, outname2, outname3, outname4, outname5, outname6):
    out_files = [open(name, 'w') for name in [outname1, outname2, outname3, outname4, outname5, outname6]]

    for number in bed_dict.keys():
        state = bed_dict[number][4]   
        
        if state in ("E3", "E4", "E5"):
            out_files[0].write('\t'.join(bed_dict[number]) + '\n')
        elif state in ("E6", "E7", "E8", "E9", "E10"):
            out_files[1].write('\t'.join(bed_dict[number]) + '\n')
        elif state in ("E1", "E2", "E12"):
            out_files[2].write('\t'.join(bed_dict[number]) + '\n')
        elif state in ("E13", "E14"):
            out_files[3].write('\t'.join(bed_dict[number]) + '\n')
        elif state in ("E15"):
            out_files[4].write('\t'.join(bed_dict[number]) + '\n')
        elif state in ("E11"):
            out_files[5].write('\t'.join(bed_dict[number]) + '\n')

    for out_file in out_files:
        out_file.close()

if __name__ == "__main__":
    infile_name1 = argv[1]   
    infile1 = open(infile_name1, 'r')   
    tissue = infile_name1.split("_")[0]   

    bed_dict = parse_bed_file(infile1, tissue)

    out_names = [f"{tissue}_{suffix}.txt" for suffix in ["TSS", "enhancer", "promoter", "repressed", "quiescent", "ATAC-island"]]

    write_output(bed_dict, tissue, *out_names)

    infile1.close()  
