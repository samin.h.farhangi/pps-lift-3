from Bio import SeqIO

def trim_headers(infile, outfile):
    with open(outfile, 'w') as out:
        for record in SeqIO.parse(infile, 'fasta'):
            record.id = record.id.split(' ')[0]  # Modify the header to keep only the part before the first space
            record.description = ''  # Clear the description
            SeqIO.write(record, out, 'fasta')  # Write the record to the output file

# call the function with your specific file names
trim_headers('Sus_scrofa.Sscrofa11.1.dna.toplevel.fa', 'sus_new.fa')

