enh=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp
DATA=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/faang_tissues.txt
out=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp

for tissue in `cat $DATA`
do
 echo $tissue
    #Enhancer: E6+E7+E8+E9+E10
	cat $enh/Pig_${tissue}_E6_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' | sort -k 1,1 -k2,2n > $out/Pig_E6_${tissue}_vep_count.txt
    cat $enh/Pig_${tissue}_E7_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' | sort -k 1,1 -k2,2n > $out/Pig_E7_${tissue}_vep_count.txt
	cat $enh/Pig_${tissue}_E8_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' | sort -k 1,1 -k2,2n > $out/Pig_E8_${tissue}_vep_count.txt
    cat $enh/Pig_${tissue}_E9_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' | sort -k 1,1 -k2,2n > $out/Pig_E9_${tissue}_vep_count.txt
    cat $enh/Pig_${tissue}_E10_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' | sort -k 1,1 -k2,2n > $out/Pig_E10_${tissue}_vep_count.txt


	#Promoter: E1+E2+E12
	cat $enh/Pig_${tissue}_E1_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' > $out/Pig_E1_${tissue}_vep_count.txt
	cat $enh/Pig_${tissue}_E2_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' > $out/Pig_E2_${tissue}_vep_count.txt
	cat $enh/Pig_${tissue}_E12_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' > $out/Pig_E12_${tissue}_vep_count.txt

	

done
