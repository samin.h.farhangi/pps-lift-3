#######################################
# Variation of regulatory elements
#PPS-LIFT project
#Samin HF, 18/05/2022
#######################################

#This pipeline provides a systematic procedure for finding the variation in regulatory elements focusing on enhancer and promoter. The steps are as follows:

#1.Genomic Data Preparation
#2.Regulatory Element (RE) Data Preparation
#3.Merging of RE and Genomic Data
#4.Identification of Target Genes of Promoters
#5.Identification of Target Genes of Enhancers
#6.CAPE Analysis for Fragile Enhancers Identification

##################################### 
#Step 1: Genomic Data Preparation
#####################################

#1A: SNP Calling
mkdir /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
#Copy all files in pps-lift-3 in RegE folder

#Counting the number of SNPs per chromosome in purbred lines used in this study 
#Input: genome seqs of five lines, LLLL; VVVV; EEEE; PPPP; ZZZZ; DDDD saved in "breed"
#Output: Pig_VVVV_num_vep.txt; Pig_LLLL_num_vep.txt; Pig_PPPP_num_vep.txt; Pig_DDDD_num_vep.txt; Pig_ZZZZ_num_vep.txt; Pig_EEEE_num_vep.txt

breed=/lustre/nobackup/WUR/ABGC/godia001/FAANG_Enhancers_Promoters/Breed.txt #LLLL; VVVV; EEEE; PPPP; ZZZZ; DDDD
for b in `cat $breed`
	do
	echo $b
	sbatch count_breed_num.sh $b
done


#1B: Merging SNP Files 
#Input: Pig_VVVV_num_vep.txt; Pig_LLLL_num_vep.txt; Pig_PPPP_num_vep.txt; Pig_DDDD_num_vep.txt; Pig_ZZZZ_num_vep.txt
#Output: Pig_breed_num_vep_merged.txt (Five columns related to each line and 19 rows related to each chromosomes)

dir = /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
paste $dir/Pig_ZZZZ_num_vep.txt $dir/Pig_VVVV_num_vep.txt $dir/Pig_EEEE_num_vep.txt $dir/Pig_PPPP_num_vep.txt $dir/Pig_LLLL_num_vep.txt $dir/Pig_DDDD_num_vep.txt > $dir/Pig_breed_num_vep.txt
echo -e "ZZZZ\tVVVV\tEEEE\tPPPP\tLLLL\tDDDD" > $dir/Pig_breed_num_vep_temp.txt 
cat $dir/Pig_breed_num_vep.txt >> $dir/Pig_breed_num_vep_temp.txt
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' $dir/Pig_breed_num_vep_temp.txt > $dir/Pig_breed_num_vep_merged.txt #Chr 19 is X
rm Pig_breed_num_vep_temp.txt

################################### 
#Step 2: RE Data Preparation
###################################

#2A: Download RE data.
#Liver_15_segments.bed, Lung_15_segments.bed, Muscle_15_segments.bed, Spleen_15_segments.bed
#Ref. Pan et al. 2021. https://doi.org/10.1038/s41467-021-26153-7
#Format of input: <chr#>   <start pos>  <End pos>  <E#>
#Data are available at https://farm.cse.ucdavis.edu/~zhypan/Nature_Communications_2021/Chromatin_State_Predictions/Pig/

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
#Save Download Liver_15_segments.bed, Lung_15_segments.bed, Muscle_15_segments.bed, Spleen_15_segments.bed in this dir  

#2B: Counting Number of RE States per Tissue
awk '{count[$NF]++} END {for (state in count) print state, count[state]}' Liver_15_segments.bed
awk '{count[$NF]++} END {for (state in count) print state, count[state]}' Lung_15_segments.bed
awk '{count[$NF]++} END {for (state in count) print state, count[state]}' Spleen_15_segments.bed
awk '{count[$NF]++} END {for (state in count) print state, count[state]}' Muscle_15_segments.bed


#2C: Separating RE Regions 
#output: Six files per tissue for 6 RE including chr stop start length tissue: 

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
   
python RE_Pan.py Liver_15_segments.bed
python RE_Pan.py Lung_15_segments.bed
python RE_Pan.py Spleen_15_segments.bed
python RE_Pan.py Muscle_15_segments.bed


#2D: Merging Promoter and Enhancer Files
cat *_promoter.txt > all_tissue_promoter.txt
cat *_enhancer.txt > all_tissue_enhancer.txt


#2E: Plotting 

#Plot the length of RE for all states and per tissue
#Input: all_tissue_Promoters.txt, all_tissue_enhancers.txt
#Output: A boxplot indicating the range of the length for each Promoter and enhancer state

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
#Promoter
awk '{NR!=0} BEGIN{print "chr\tstart\tstop\tlen\tstate\ttissue"}; {print}' all_tissue_promoter.txt > tmp1.txt  
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' tmp1.txt > tmp2.txt  
awk '{print $7"\t"$1"\t"$4"\t"$5"\t"$6}' tmp2.txt> promoter.txt
#Enhancer
awk '{NR!=0} BEGIN{print "chr\tstart\tstop\tlen\tstate\ttissue"}; {print}' all_tissue_enhancer.txt > tmp1.txt  
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' tmp1.txt > tmp2.txt  
awk '{print $7"\t"$1"\t"$4"\t"$5"\t"$6}' tmp2.txt> enhancer.txt
rm tmp*.txt

#--------------------------------------------------
module load R/4.0.2
R
install.packages("ggplot2")
library("ggplot2")
library("dplyr")

#Promoter
Promoter_data <- read.table("promoter.txt", header = TRUE, row.names = 1)
#Range of length RE without outliers
ggplot(Promoter_data, aes(x=state, y=len)) + 
  geom_boxplot(fill="green", color="black", outlier.shape = NA)+
  xlab("Segments") + 
  ylab("Length") +
  ylim(0, 4000) +
  scale_x_discrete(limits = c("E1", "E2", "E12"))
#Comparing the length per tissue; Without outliers
p<-ggplot(Promoter_data, aes(x = state, y = len, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Segments") + 
  ylab("Length") +  
  ylim(0,4000)  +
  scale_x_discrete(limits = c("E1", "E2", "E12")) 
png("Promoter_length.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()
    
#Enhancer
enhancer_data <- read.table("enhancer.txt", header = TRUE, row.names = 1)
#Range of length RE without outliers
ggplot(enhancer_data, aes(x=state, y=len)) + 
  geom_boxplot(fill="gray", color="black", outlier.shape = NA)+
  xlab("Segments") + 
  ylab("Length") +
  ylim(0, 4000) +
  scale_x_discrete(limits = c("E6", "E7", "E8", "E9", "E10"))
#Comparing the length per tissue; Without outliers
p<-ggplot(enhancer_data, aes(x = state, y = len, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Segments") + 
  ylab("Length") +  
  ylim(0,4000) +
  scale_x_discrete(limits = c("E6", "E7", "E8", "E9", "E10"))
png("Enhancer_length.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()
#-------------------------------------------------------------
  
#2F: Coverage Analysis
#Coverage of RE states per tissue over the genome
#--------------------------------------------------------------
module load R/4.0.2
R

# Coverage of genome by Promoters per each state
Promoter <- read.table("promoter.txt", header = TRUE, row.names = 1)
calculate_coverage <- function(tissue_name) {
  tissue <- filter(Promoter, tissue == tissue_name)
  E1 <- filter(tissue, state == "E1")
  E2 <- filter(tissue, state == "E2")
  E12 <- filter(tissue, state == "E12")
  
  total_len <- sum(E1$len) + sum(E2$len) + sum(E12$len)
  genome_size <- 2364710000
  
  print(paste("Coverage of genome by Promoter in", tissue_name, ": ", (total_len * 100) / genome_size))
  print(paste("Coverage of genome by E1 in", tissue_name, ": ", (sum(E1$len) * 100) / genome_size))
  print(paste("Coverage of genome by E2 in", tissue_name, ": ", (sum(E2$len) * 100) / genome_size))
  print(paste("Coverage of genome by E12 in", tissue_name, ": ", (sum(E12$len) * 100) / genome_size))
}
calculate_coverage("Liver")
calculate_coverage("Lung")
calculate_coverage("Spleen")
calculate_coverage("Muscle")

# Coverage of genome by Enhancers per each state
Enhancer <- read.table("enhancer.txt", header = TRUE, row.names = 1)
calculate_coverage <- function(tissue_name) {
  tissue <- filter(Enhancer, tissue == tissue_name)
  E6 <- filter(tissue, state == "E6")
  E7 <- filter(tissue, state == "E7")
  E8 <- filter(tissue, state == "E8")
  E9 <- filter(tissue, state == "E9")
  E10 <- filter(tissue, state == "E10")

  total_len <- sum(E6$len) + sum(E7$len) + sum(E8$len) + sum(E9$len) + sum(E10$len)
  genome_size <- 2364710000
  
  print(paste("Coverage of genome by Enhancer in", tissue_name, ": ", (total_len * 100) / genome_size))
  print(paste("Coverage of genome by E6 in", tissue_name, ": ", (sum(E6$len) * 100) / genome_size))
  print(paste("Coverage of genome by E7 in", tissue_name, ": ", (sum(E7$len) * 100) / genome_size))
  print(paste("Coverage of genome by E8 in", tissue_name, ": ", (sum(E8$len) * 100) / genome_size))
  print(paste("Coverage of genome by E9 in", tissue_name, ": ", (sum(E9$len) * 100) / genome_size))
  print(paste("Coverage of genome by E10 in", tissue_name, ": ", (sum(E10$len) * 100) / genome_size))

}
calculate_coverage("Liver")
calculate_coverage("Lung")
calculate_coverage("Spleen")
calculate_coverage("Muscle")
#-------------------------------------------------------------------------------

########################################### 
#Step 3: Merging RE Data and Genomic Data
###########################################

#3A: SNP Density within RE Regions
# Based on the SNP detected in pure breeds (1A) 
#Input: RE files from Pan et al.: Liver_15_segments.bed, Lung_15_segments.bed, Muscle_15_segments.bed, Spleen_15_segments.bed
#output: The full file of each tissue has been devided into 14 files per each tissue including one E segments named "Pig_<tissue name>_E#" e.g. Pig_Liver_E9

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/

awk -F'\t' '{print>"Pig_Liver_"$4;}' Liver_15_segments.bed
awk -F'\t' '{print>"Pig_Lung_"$4;}' Lung_15_segments.bed
awk -F'\t' '{print>"Pig_Muscle_"$4;}' Muscle_15_segments.bed
awk -F'\t' '{print>"Pig_Spleen_"$4;}' Spleen_15_segments.bed


#Add a column with length of RE
bash count_length.sh

#Clean-up file and prepare bed format (remove chr)
#Input: Pig_Liver_${i}_length e.g. Pig_Liver_E1_length  
#Output: bed file named e.g.Pig_Liver_E1_length_up.bed
bash clean_up.sh


#Check the overlap between found SNPs and chromatin states
#Input: e.g.Pig_Liver_E1_length_up.bed
#output: e.g. Pig_E1_vep_count_temp.txt
bash overlap_count.sh


#Extract number of variants per tissue and per segments in all files 
#Input: e.g. Pig_E1_vep_count_temp.txt
#Output: Pig_Enhancer_vep_count.txt and Pig_Promoter_vep_count.txt

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/

#Enhancer: E6+E7+E8+E9+E10
cat Pig_*_E6_vep_count_temp.txt Pig_*_E7_vep_count_temp.txt Pig_*_E8_vep_count_temp.txt Pig_*_E9_vep_count_temp.txt Pig_*_E10_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' > Pig_Enhancer_vep_count.txt
#Promoter: E1+E2+E12
cat Pig_*_E1_vep_count_temp.txt Pig_*_E2_vep_count_temp.txt Pig_*_E12_vep_count_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' > Pig_Promoter_vep_count.txt



#R ploting: Number of variants per regulatory elements
#Input: Pig_Enhancer_vep_count.txt; Pig_Promoter_vep_count.txt

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/

#Promoter
awk '{NR!=0} BEGIN{print "chr\tstart\tend\tstate\ttissue\tvalue"}; {print}'  Pig_Promoter_vep_count.txt > tmp1.txt
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' tmp1.txt > tmp2.txt
awk '{print $7"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' tmp2.txt > Pig_Promoter_vep_plot.txt

#Enhancer
awk '{NR!=0} BEGIN{print "chr\tstart\tend\tstate\ttissue\tvalue"}; {print}'  Pig_Enhancer_vep_count.txt > tmp1.txt
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' tmp1.txt > tmp2.txt
awk '{print $7"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' tmp2.txt > Pig_Enhancer_vep_plot.txt

rm tmp*.txt

#-----------------------------------------------------------------------------------------
module load R/4.0.2
R
install.packages("ggplot2")
library("ggplot2")
library(dplyr)

# SNP density in promoter
my_data_pro <- read.table("Pig_Promoter_vep_plot.txt", header = TRUE, row.names = 1)
df_pro <- data.frame(my_data_pro)
df_pro['SNP_1Kb'] <- df_pro$value*1000

density_pro <- function(tissue_name) {
  tissue <- filter(df_pro, tissue == tissue_name)
  E1 <- filter(tissue, state =="E1")
  E2 <- filter(tissue, state =="E2")
  E12 <- filter(tissue, state =="E12")
  print(paste("Mean SNP/1kb in E1", tissue_name, ": ", mean(E1$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E2", tissue_name, ": ", mean(E2$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E12", tissue_name, ": ", mean(E12$SNP_1Kb)))
}

density_pro("Liver")
density_pro("Lung")
density_pro("Spleen")
density_pro("Muscle")

p <- ggplot(df_pro, aes(x = state, y = SNP_1Kb, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Promoters") + 
  ylab("SNP density / 1kb") +
  ylim(0,25) +
  scale_x_discrete(limits = c("E1", "E2", "E12"))

png("promoter_density.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

# SNP density in enhancer
my_data_enh <- read.table("Pig_Enhancer_vep_plot.txt", header = TRUE, row.names = 1)
df_enh <- data.frame(my_data_enh)
df_enh['SNP_1Kb'] <- df_enh$value*1000
density_enh <- function(tissue_name) {
  tissue <- filter(df_enh, tissue == tissue_name)
  E6 <- filter(tissue, state =="E6")
  E7 <- filter(tissue, state =="E7")
  E8 <- filter(tissue, state =="E8")
  E9 <- filter(tissue, state =="E9")
  E10 <- filter(tissue, state =="E10")
  print(paste("Mean SNP/1kb in E6", tissue_name, ": ", mean(E6$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E7", tissue_name, ": ", mean(E7$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E8", tissue_name, ": ", mean(E8$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E9", tissue_name, ": ", mean(E9$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E10", tissue_name, ": ", mean(E10$SNP_1Kb)))
}
density_enh("Liver")
density_enh("Lung")
density_enh("Spleen")
density_enh("Muscle")


#Plot without outliers
p<-ggplot(df_enh, aes(x = state, y = SNP_1Kb, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Enhancers") + 
  ylab("SNP density / 1kb") +  
  ylim(0,25)+
  scale_x_discrete(limits = c("E6", "E7", "E8", "E9", "E10"))
png("enhancer_density.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()
#-------------------------------------------------------------------------------------------

#3B: SNP Density of Random Coordinates within RE regions
 
#Extract random coordinates from the genome
bash random_extract.sh

#Count variants of random places
bash random_variants.sh

#Extract number of random variants per tissue and per segments in all files(enhancers and promoters)
#Input: e.g. Pig_E1_count_random_temp.txt
#Output: Pig_Enhancer_count_random.txt and Pig_Promoter_count_random.txt

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
#Enhancer: E6+E7+E8+E9+E10
cat Pig_*_E6_vep_count_random_temp.txt Pig_*_E7_vep_count_random_temp.txt Pig_*_E8_vep_count_random_temp.txt Pig_*_E9_vep_count_random_temp.txt Pig_*_E10_vep_count_random_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' > Pig_Enhancer_vep_count_random.txt
#Promoter: E1+E2+E12
cat Pig_*_E1_vep_count_random_temp.txt Pig_*_E2_vep_count_random_temp.txt Pig_*_E12_vep_count_random_temp.txt | awk '{print $2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$8}' > Pig_Promoter_vep_count_random.txt

#R ploting: Number of random variants per regulatory elements
#Input: Pig_Enhancer_vep_random_count.txt; Pig_Promoter_vep_random_count.txt

cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/

#Promoter
awk '{NR!=0} BEGIN{print "chr\tstart\tend\tstate\ttissue\tvalue"}; {print}'  Pig_Promoter_vep_count_random.txt > tmp1.txt
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' tmp1.txt > tmp2.txt
awk '{print $7"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' tmp2.txt > ./Pig_Promoter_random_vep_plot.txt

#Enhancer
awk '{NR!=0} BEGIN{print "chr\tstart\tend\tstate\ttissue\tvalue"}; {print}'  Pig_Enhancer_vep_count_random.txt > tmp1.txt
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' tmp1.txt > tmp2.txt
awk '{print $7"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' tmp2.txt > ./Pig_Enhancer_random_vep_plot.txt

rm tmp*.txt

#-----------------------------------------------------------------------------------------
module load R/4.0.2
R
library("ggplot2")
library(dplyr)

# SNP density in promoter
my_data_pro <- read.table("Pig_Promoter_random_vep_plot.txt", header = TRUE, row.names = 1)
df_pro <- data.frame(my_data_pro)
df_pro['SNP_1Kb'] <- df_pro$value*1000

density_pro <- function(tissue_name) {
  tissue <- filter(df_pro, tissue == tissue_name)
  E1 <- filter(tissue, state =="E1")
  E2 <- filter(tissue, state =="E2")
  E12 <- filter(tissue, state =="E12")
  print(paste("Mean SNP/1kb in E1", tissue_name, ": ", mean(E1$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E2", tissue_name, ": ", mean(E2$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E12", tissue_name, ": ", mean(E12$SNP_1Kb)))
}

density_pro("Liver")
density_pro("Lung")
density_pro("Spleen")
density_pro("Muscle")

p <- ggplot(df_pro, aes(x = state, y = SNP_1Kb, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Promoters") + 
  ylab("SNP density / 1kb") +
  ylim(0,25) +
  scale_x_discrete(limits = c("E1", "E2", "E12"))

png("promoter_random_density.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

# SNP density in enhancer
my_data_enh <- read.table("Pig_Enhancer_random_vep_plot.txt", header = TRUE, row.names = 1)
df_enh <- data.frame(my_data_enh)
df_enh['SNP_1Kb'] <- df_enh$value*1000
density_enh <- function(tissue_name) {
  tissue <- filter(df_enh, tissue == tissue_name)
  E6 <- filter(tissue, state =="E6")
  E7 <- filter(tissue, state =="E7")
  E8 <- filter(tissue, state =="E8")
  E9 <- filter(tissue, state =="E9")
  E10 <- filter(tissue, state =="E10")
  print(paste("Mean SNP/1kb in E6", tissue_name, ": ", mean(E6$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E7", tissue_name, ": ", mean(E7$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E8", tissue_name, ": ", mean(E8$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E9", tissue_name, ": ", mean(E9$SNP_1Kb)))
  print(paste("Mean SNP/1kb in E10", tissue_name, ": ", mean(E10$SNP_1Kb)))
}
density_enh("Liver")
density_enh("Lung")
density_enh("Spleen")
density_enh("Muscle")


#Plot without outliers
p<-ggplot(df_enh, aes(x = state, y = SNP_1Kb, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Enhancers") + 
  ylab("SNP density / 1kb") +  
  ylim(0,25)+
  scale_x_discrete(limits = c("E6", "E7", "E8", "E9", "E10"))
png("enhancer_random_density.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()
#-------------------------------------------------------------------------------------------

#3C. Statistical Analysis: summary
#Output: Summary_statistics_num_variants.txt
bash stat_G3.sh


#3D. Statistical Analysis: Wilcoxon Test
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/

#--------------------------------------------------
module load R/4.0.2
R
tissue <- list("Liver", "Lung", "Spleen","Muscle")
segment_p <- list("E1","E2","E12") #promoter
segment_e <- list("E6","E7","E8","E9", "E10") #Enhancer

for (i in tissue) { 
  for (x in segment_p) {
    print(paste(i,x),sep="")
    file_1 <- read.table(paste("Pig_",i,"_",x,"_vep_count_temp.txt",sep=""), h=F)
    file_2 <- read.table(paste("Pig_",i,"_",x,"_vep_count_random_temp.txt",sep=""), h = F)
    print(dim(file_1))
    res <- wilcox.test(file_1$V8, file_2$V8)
    print(res$p.value)
  }
}

for (i in tissue) { 
  for (x in segment_e) {
    print(paste(i,x),sep="")
    file_1 <- read.table(paste("Pig_",i,"_",x,"_vep_count_temp.txt",sep=""), h=F)
    file_2 <- read.table(paste("Pig_",i,"_",x,"_vep_count_random_temp.txt",sep=""), h = F)
    print(dim(file_1))
    res <- wilcox.test(file_1$V8, file_2$V8)
    print(res$p.value)
  }
}
q()
#------------------------------------------


#3E. eGWAS-SNP Density within RE Regions

#Creating a file per tissue
mv prom_enh_states.txt /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp
 
#Input: e.g. Pig_Liver_E7_vep_count_temp.txt 
#Output: ptomotor and enhancer file per tissue, e.g. Pig_E1_Liver_vep_count.txt  

bash RE_seg_per_tissue.sh


#Linking between significant SNP positions from eGWAS and  RE states 
#Input: 1)Pig_${RE}_${tissue}_vep_count.txt (3.2.A.); 2)filtered_signBH_genes.txt (resulted from eGWAS)
#Output: {RE}_overlap.txt

#Liver
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp
mkdir 1.Liver_seg
cd 1.Liver_seg
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS/eGWAS_filter/filtered_Liver_signBH_genes.txt ./
awk '{print $4"\t"$5"\t"$5"\t"$6}' filtered_Liver_signBH_genes.txt > filtered_Liver_signBH_genes.bed #Converting txt to bed file 
bash RE_seg_eGWAS_Liver.sh

#Spleen
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp
mkdir 2.Spleen_seg
cd 2.Spleen_seg
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS/eGWAS_filter/filtered_Spleen_signBH_genes.txt ./
awk '{print $4"\t"$5"\t"$5"\t"$6}' filtered_Spleen_signBH_genes.txt > filtered_Spleen_signBH_genes.bed
bash RE_seg_eGWAS_Spleen.sh

#Lung
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp
mkdir 3.Lung_seg
cd 3.Lung_seg
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS/eGWAS_filter/filtered_Lung_signBH_genes.txt ./
awk '{print $4"\t"$5"\t"$5"\t"$6}' filtered_Lung_signBH_genes.txt > filtered_Lung_signBH_genes.bed
bash RE_seg_eGWAS_Lung.sh


#Muscle 
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/2.RegE/egwas_snp
mkdir 4.Muscle_seg
cd 4.Muscle_seg
awk '{print $4"\t"$5"\t"$5"\t"$6}' filtered_Muscle_signBH_genes.txt > filtered_Muscle_signBH_genes.bed
bash RE_seg_eGWAS_Muscle.sh

#3F. Mean of 100 Random-Count

#Liver:
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/2.RegE/egwas_snp/1.Liver
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E1_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E2_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E12_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E6_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E7_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E8_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E9_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E10_overlap.txt

#Spleen: 
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/2.RegE/egwas_snp/2.Spleen
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E1_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E2_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E12_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E6_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E7_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E8_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E9_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E10_overlap.txt

#Lung: 
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/2.RegE/egwas_snp/3.Lung
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E1_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E2_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E12_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E6_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E7_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E8_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E9_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E10_overlap.txt

#Muscle: 
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/2.RegE/egwas_snp/4.Muscle
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E1_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E2_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E12_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E6_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E7_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E8_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E9_overlap.txt
awk '{ sum += $1 } END { if (NR > 0) print sum / NR }' E10_overlap.txt


#3G: One-sample t-test 
#-----------------------------------------------------------------------------------------
module load R/4.0.2
R
setwd("/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp/1.Liver")
#t-test.R
my_data <- read.delim("E1_overlap.txt", h=FALSE) #E2, E12, E6, E7, E8, E9, E10
count <- read.delim("Number_E1_found_shared.txt", h=FALSE) #E2, E12, E6, E7, E8, E9, E10
colnames(my_data) <- c("compare")
num <- count$V1
res <- t.test(my_data$compare, mu = num)
res$p.value

setwd("/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp/2.Spleen")
my_data <- read.delim("E1_overlap.txt", h=FALSE) #E2, E12, E6, E7, E8, E9, E10
count <- read.delim("Number_E1_found_shared.txt", h=FALSE) #E2, E12, E6, E7, E8, E9, E10
colnames(my_data) <- c("compare")
num <- count$V1
res <- t.test(my_data$compare, mu = num)
res$p.value

setwd("/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp/3.Lung")
my_data <- read.delim("E1_overlap.txt", h=FALSE) #E2, E12, E6, E7, E8, E9, E10
count <- read.delim("Number_E1_found_shared.txt", h=FALSE) #E2, E12, E6, E7, E8, E9, E10
colnames(my_data) <- c("compare")
num <- count$V1
res <- t.test(my_data$compare, mu = num)
res$p.value

setwd("/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp/4.Muscle")
my_data <- read.delim("E1_overlap.txt", h=FALSE) #E2, E12, E6, E7, E8, E9, E10
count <- read.delim("Number_E1_found_shared.txt", h=FALSE) #E2, E12, E6, E7, E8, E9, E10
colnames(my_data) <- c("compare")
num <- count$V1
res <- t.test(my_data$compare, mu = num)
res$p.value
#-----------------------------------------------------------------------------------------

#################################################### 
#Step 4: Identification of Target Genes of Promoters
####################################################

#4A: Extraction and annotation of Tissue-specific genes

#Extraction:

#Convert input into 4 tissue files
#Input is Final_counts_CPM.txt resulted from RNAseq (pps-lift-1)
mkdir Promoter_target_genes
cd Promoter_target_genes
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/4.Quantification/Final_counts_CPM ./

python CPM_norm_tissues.py

#---------------------------------------------------------------------------------------------------
module load R/4.0.2
R
library("DESeq2")
library("edgeR")
library("limma")
library("ggplot2")
library("pheatmap")
library("matrixStats")

#Read the seperate count CPM file per each tissue (A:Liver; B:Spleen; C:Lung; D:Muscle)
A_cpm <- read.table("A_counts_CPM.txt", header = TRUE, row.names = 1)
B_cpm <- read.table("B_counts_CPM.txt", header = TRUE, row.names = 1)
C_cpm <- read.table("C_counts_CPM.txt", header = TRUE, row.names = 1)
D_cpm <- read.table("D_counts_CPM.txt", header = TRUE, row.names = 1)

#Calcilate SD and mean of each gene
data_mean_SD <- cbind(rowMeans(A_cpm),rowSds(as.matrix(A_cpm)),
                      rowMeans(B_cpm),rowSds(as.matrix(B_cpm)),
                      rowMeans(C_cpm),rowSds(as.matrix(C_cpm)),
                      rowMeans(D_cpm),rowSds(as.matrix(D_cpm)))
colnames(data_mean_SD) <- c("Mean_Liver","SD_Liver",
                            "Mean_Spleen","SD_Spleen",
                            "Mean_Lung","SD_Lung",
                            "Mean_Muscle","SD_Muscle")
							
#Writing the Final_mean_SD_CPM.txt including SD and mean of CPM per each tissue							
write.table(as.data.frame(data_mean_SD), file="./Final_mean_SD_CPM.txt", quote = FALSE, sep="\t")            


# Extraction of tissue-specific expression if they were expressed at least 4-fold higher than all other tissues.
new_mean <- (data_mean_SD)+0.0001 #Addition of 0.0001 for avoiding 0 divisions
 
ratios_Liver <- cbind(new_mean[,1],(new_mean[,1]/new_mean[,3]),
                      (new_mean[,1]/new_mean[,5]),
       
	   (new_mean[,1]/new_mean[,7]))
colnames(ratios_Liver) <- c("Mean_Liver","Liver/Spleen","Liver/Lung","Liver/Muscle")

ratios_Spleen <- cbind(new_mean[,3],(new_mean[,3]/new_mean[,1]),
                     (new_mean[,3]/new_mean[,5]),
                     (new_mean[,3]/new_mean[,7]))
colnames(ratios_Spleen) <- c("Mean_Spleen","Spleen/Liver","Spleen/Lung","Spleen/Muscle")

ratios_Lung <- cbind(new_mean[,5],(new_mean[,5]/new_mean[,1]),
                       (new_mean[,5]/new_mean[,3]),
                       (new_mean[,5]/new_mean[,7]))
colnames(ratios_Lung) <- c("Mean_Lung","Lung/Liver","Lung/Spleen","Lung/Muscle")

ratios_Muscle <- cbind(new_mean[,7],(new_mean[,7]/new_mean[,1]),
                       (new_mean[,7]/new_mean[,3]),
                       (new_mean[,7]/new_mean[,5]))
colnames(ratios_Muscle) <- c("Mean_Muscle","Muscle/Liver","Muscle/Spleen","Muscle/Lung")


#Extract if there is a 4 FC increase in the 3 different comparisons and then filter by at least 1 CPM of the given tissue
#Liver
ratios_Liver_count <- cbind(ratios_Liver,(as.data.frame(rowSums(ratios_Liver[,-1] >= 4))[,1]))
colnames(ratios_Liver_count)[ncol(ratios_Liver_count)] <- c("count")
Liver_specific <- (ratios_Liver_count[which(ratios_Liver_count[,5]==3 & ratios_Liver_count[,1]>=1),])[,-ncol(ratios_Liver_count)]
dim(Liver_specific)

#Spleen
ratios_Spleen_count <- cbind(ratios_Spleen,(as.data.frame(rowSums(ratios_Spleen[,-1] >= 4))[,1]))
colnames(ratios_Spleen_count)[ncol(ratios_Spleen_count)] <- c("count")
Spleen_specific <- (ratios_Spleen_count[which(ratios_Spleen_count[,5]==3 & ratios_Spleen_count[,1]>=1),])[,-ncol(ratios_Spleen_count)]
dim(Spleen_specific)

#Lung
ratios_Lung_count <- cbind(ratios_Lung,(as.data.frame(rowSums(ratios_Lung[,-1] >= 4))[,1]))
colnames(ratios_Lung_count)[ncol(ratios_Lung_count)] <- c("count")
Lung_specific <- (ratios_Lung_count[which(ratios_Lung_count[,5]==3 & ratios_Lung_count[,1]>=1),])[,-ncol(ratios_Lung_count)]
dim(Lung_specific)

#Muscle
ratios_Muscle_count <- cbind(ratios_Muscle,(as.data.frame(rowSums(ratios_Muscle[,-1] >= 4))[,1]))
colnames(ratios_Muscle_count)[ncol(ratios_Muscle_count)] <- c("count")
Muscle_specific <- (ratios_Muscle_count[which(ratios_Muscle_count[,5]==3 & ratios_Muscle_count[,1]>=1),])[,-ncol(ratios_Muscle_count)]
dim(Muscle_specific)

#Save the specific genes per each tissue
write.table(as.data.frame(Liver_specific), file="Liver_specific.txt", quote = FALSE, sep="\t")
write.table(as.data.frame(Lung_specific), file="Lung_specific.txt", quote = FALSE, sep="\t")
write.table(as.data.frame(Spleen_specific), file="Spleen_specific.txt", quote = FALSE, sep="\t")
write.table(as.data.frame(Muscle_specific), file="Muscle_specific.txt", quote = FALSE, sep="\t")
#--------------------------------------------------------------------------------------------


#Annotatation:
 
#Cleanup the bed file of pig created in pps-lift2-4A
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS/eGWAS_filter/Sscrofa11.1.ens.104.bed ./
awk 'NR>1{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7}' Sscrofa11.1.ens.104.bed > Sscrofa11.1.ens.104_cleanup.bed

#Extract coordinates of the tissue-specific genes (in any of the 4 tissues-they are >1 CPM)
bash coordinate_tissue_specific.sh

#Merge all tissue-specific genes
cat Liver_specific_coordinates.txt Lung_specific_coordinates.txt Muscle_specific_coordinates.txt Spleen_specific_coordinates.txt > All_tissues_merged_coordinates.txt 

#Make sure no genes are found in more than 1 tissue
awk '{print $5}' All_tissues_merged_coordinates.txt | sort | uniq -c | sort -nr | tail

#Extract protein-coding genes in +/- strands from gene-specific genes
#output: {tissue}_specific_coordinates_sorted.txt; {tissue}_specific_coordinates_sorted_rev.txt; {tissue}_specific_coordinates_sorted_fwd.txt
bash specific_sep.sh

#4B: Extraction and annotation of Non-tissue-specific genes

#Extraction:
 
#Non-tissue-specific are the genes which were expressed > 1 CPM mean in all tissues
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/
awk '{if ($2>=1 && $4>=1 && $6>=1 && $8>=1) print $1}' Final_mean_SD_CPM.txt | grep -v "Mean" > Genes_over_1_CPM.txt

#Annotatation:

#Extract the coordinates of these non-specific genes
awk 'NR==FNR{a[$1]=$0} NR>FNR && a[$5]>0{print $0}' Genes_over_1_CPM.txt Sus_scrofa.Sscrofa11.1.104.bed > Genes_over_1_CPM_temp.txt


#4C: Extraction of Houskeeping genes (HK genes)

#Extraction:

#Extracted from Wei et al. 2018 (10.7717/peerj.4840/supp-10)

#Annotation
#Extract the coordinates of HK genes 
awk 'NR==FNR{a[$1]=$0} NR>FNR && a[$5]>0{print $0}' HK_wei2018.txt Sus_scrofa.Sscrofa11.1.104.bed > wei_housekeeping.txt
#Extract protein-coding genes in +/- strands from houskeeping genes
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/
awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' wei_housekeeping.txt | sort -k 1,1 -k2,2n > wei_housekeeping.bed
#Extract only the protein_coding genes
grep "protein_coding" wei_housekeeping.bed > wei_housekeeping_protein_coding.bed
#Separate gene by + or - strand
awk '($4=="+")' wei_housekeeping_protein_coding.bed > wei_housekeeping_protein_coding_fwd.txt
awk '($4=="-")' wei_housekeeping_protein_coding.bed > wei_housekeeping_protein_coding_rev.txt

#4D: Extraction of the nearest 5'promoter
#Output: E#_genes_housekeeping_${tissue}_merged.txt and E#_genespecific_${tissue}_merged.txt (E1, E2, E12)
bash extract_promoter.sh

#4E: Calculation of the distance of promoters from genes
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/

#Merge all files
cat E*_genespecific_*_merged.txt > Promoters_genespecific_all_tissues_merged.txt
cat E*_genes_housekeeping_*_merged.txt > Promoters_genes_housekeeping_all_tissues_merged.txt

#Ploting the distance
awk '{NR!=0} BEGIN{print "chr\tstart\tend\tstate\ttissue\tvalue\tdistance\tgene_id"}; {print}'  Promoters_genespecific_all_tissues_merged.txt > tmp1.txt
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' tmp1.txt > tmp2.txt
awk '{print $9"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8}' tmp2.txt > Promoters_genespecific_all_tissues_plot.txt
#remove some line with < 8 elements (first line has 8 elemnts and others 9 elements)
awk 'NF == 8 || NF == 9' Promoters_genespecific_all_tissues_plot.txt > Promoters_genespecific_all_tissues_edit_plot.txt


awk '{NR!=0} BEGIN{print "chr\tstart\tend\tstate\ttissue\tvalue\tdistance\tgene_id"}; {print}'  Promoters_genes_housekeeping_all_tissues_merged.txt > tmp1.txt
awk -F'\t' 'NR>1{$0=$0"\t"NR-1} 1' tmp1.txt > tmp2.txt
awk '{print $9"\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8}' tmp2.txt > Promoters_genes_housekeeping_all_tissues_plot.txt
#remove some line with < 8 elements (first line has 8 elemnts and others 9 elements)
awk 'NF == 8 || NF == 9' Promoters_genes_housekeeping_all_tissues_plot.txt > Promoters_genes_housekeeping_all_tissues_edit_plot.txt


rm tmp*
rm Promoters_genespecific_all_tissues_plot.txt
rm Promoters_genes_housekeeping_all_tissues_plot.txt

#------------------------------------------------------------------------------
module load R/4.0.2
R
install.packages("ggplot2")
library("ggplot2")
library(dplyr)


my_data_hk <- read.table("Promoters_genes_housekeeping_all_tissues_edit_plot.txt", header = TRUE, row.names = 1)
my_data_sp <- read.table("Promoters_genespecific_all_tissues_edit_plot.txt", header = TRUE, row.names = 1)

df_hk <- data.frame(my_data_hk)
df_sp <- data.frame(my_data_sp)


# Create SNP density plot
#SNP density in 1kb 
df_hk['SNP_1Kb'] <- df_hk$value*1000
df_sp['SNP_1Kb'] <- df_sp$value*1000


#Plot Without outliers
#HK
p<-ggplot(df_hk, aes(x = state, y = SNP_1Kb, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Promoters of housekeeping") + 
  ylab("SNP density / 1kb")+
  ylim(0,25) +
  scale_x_discrete(limits = c("E1", "E2", "E12"), labels = c("TssA", "TssAHet", "TssBiv"))
png("snp_density_promoter_hk.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

#Specific
p<-ggplot(df_sp, aes(x = state, y = SNP_1Kb, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Promoters of Specific genes") + 
  ylab("SNP density / 1kb")+
  ylim(0,25) +
  scale_x_discrete(limits = c("E1", "E2", "E12"), labels = c("TssA", "TssAHet", "TssBiv"))
png("snp_density_promoter_specific.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

# Calculate mean of SNP_1Kb
#For df_hk (housekeeping genes)
df_hk_means <- df_hk %>%
  filter(tissue %in% c("Liver", "Lung", "Spleen", "Muscle")) %>%
  group_by(tissue, state) %>%
  summarise(mean_SNP_1Kb = mean(SNP_1Kb), .groups = 'drop')
df_hk_means
# For df_sp (specific genes)
df_sp_means <- df_sp %>%
  filter(tissue %in% c("Liver", "Lung", "Spleen", "Muscle")) %>%
  group_by(tissue, state) %>%
  summarise(mean_SNP_1Kb = mean(SNP_1Kb), .groups = 'drop')
df_sp_means

# Create the density plot for distance
# Combine data frames and add a 'source' column

df_hk$source <- "housekeeping"
df_sp$source <- "specific"
combined_df <- rbind(df_hk, df_sp)

# Create the density plot
p<-ggplot(combined_df, aes(x = distance, fill = source, color = source, linetype = source)) +
  geom_density(alpha = 0.4) +
  xlab("Distance") +
  ylab("Density") +
  theme_minimal() +
  labs(title = "Density plot of distances for housekeeping and specific genes")
png("hk_specific_distance_promoter.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

  
#difference between variation in hk and sp
# Combine the mean SNP data
df_means <- merge(df_hk_means, df_sp_means, by = c("tissue", "state"), suffixes = c("_hk", "_sp"))

# Calculate the difference in mean SNP density
df_means$mean_diff = df_means$mean_SNP_1Kb_sp - df_means$mean_SNP_1Kb_hk

# Print the results
print(df_means)
  
#------------------------------------------------------------------------------

####################################################  
#Step 5: Identification of Target Genes of Enhancers
####################################################  

#5A: Extract the target genes based on H3K27ac_CTCFTAD files
#Extract the RE segment of enhancer from Pig_Enhancer_vep_count.txt
#All txt files converted first to excel file

mkdir /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Enhancer_target_genes

dir=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
dir_enhancer=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Enhancer_target_genes/

awk '{print $4}' $dir/Pig_Enhancer_vep_count.txt | sort | uniq -c
awk '{print $5}' $dir/Pig_Enhancer_vep_count.txt | sort | uniq -c
awk '{print $1":"$2"-"$3"\t"$4"\t"$5"\t"$6}' $dir/Pig_Enhancer_vep_count.txt > $dir_enhancer/Pig_Enhancer_vep_count_cleanup.txt
  
#5B: Extraction of tissue-specific and HK target genes
#------------------------------------------------------------------------------
module load R/4.0.2
R
install.packages("ggplot2")
library("ggplot2")
library(dplyr)
library("readxl")
library(openxlsx)
library(stringr)
  
  
#Read the excel fils
vep <- read_excel("pan.xlsx", sheet="vep") #Pig_Enhancer_vep_count_cleanup.txt
e6 <- read_excel("pan.xlsx", sheet="E6") #output_E6_confident from pan et al.
e7 <- read_excel("pan.xlsx", sheet="E7") #output_E7_confident from pan et al.
e8 <- read_excel("pan.xlsx", sheet="E8") #output_E8_confident from pan et al.
e9 <- read_excel("pan.xlsx", sheet="E9") #output_E9_confident from pan et al.
e10 <- read_excel("pan.xlsx", sheet="E10") #output_E10_confident from pan et al.
  
merged_enhancer <- bind_rows(e6, e7, e8, e9, e10)
#Remove "chr"
merged_enhancer$RE <- str_replace(merged_enhancer$RE, "chr", "")
  
# Merge the two data frames based on the "RE" column (location of RE)
merged_vep_enhancer <- merge(merged_enhancer, vep, by = "RE")

write.xlsx(merged_vep_enhancer, "merged_vep_enhancer.xlsx",row.names = FALSE)

#Info about enhancer target genes
#Length of enhancers
data_frames = list(e6, e7, e8, e9, e10)
lengths = lapply(data_frames, function(df) length(df$RE))
print(lengths)

# Count unique values in the first column (RE)
data_frames = list(e6, e7, e8, e9, e10)
results = lapply(data_frames, function(df) {
  unique_RE_count <- df %>% count(RE)
  list(unique_count = nrow(unique_RE_count),
       mean_count = mean(unique_RE_count$n))
})
results


# Count unique values in the fourth column (gene)
data_frames = list(e6, e7, e8, e9, e10)
results = lapply(data_frames, function(df) {
  unique_gene_count <- df %>% count(gene)
  list(unique_count = nrow(unique_gene_count),
       mean_count = mean(unique_gene_count$n))
})
results

#Houskeeping enhancer target genes
# Read the wei_housekeeping file
wei_housekeeping <- read_excel("target_gene_RE_count.xlsx", sheet="wei_housekeeping") #wei_housekeeping.txt
  
# Define the tissues
tissues <- c("Liver", "Lung", "Spleen", "Muscle")
  
# Process each tissue
for (tissue in tissues) {
  enhancer_data <- merged_vep_enhancer[merged_vep_enhancer$tissue == tissue, ]
    
  matching_genes <- enhancer_data[enhancer_data$gene %in% wei_housekeeping$gene, "gene"]
    
  matching_info <- wei_housekeeping[wei_housekeeping$gene %in% matching_genes, c("gene", "biotype", "gene_name")]
    
  subset_enhancer <- enhancer_data[enhancer_data$gene %in% matching_genes, ]
  HK_enhancer <- merge(subset_enhancer, matching_info, by = "gene", all.x = TRUE)
    
  write.xlsx(HK_enhancer, paste0("HK_", tolower(tissue), "_enhancer.xlsx"),row.names = FALSE)
}
  
merged_data_HK <- data.frame()
tissues <- c("Liver", "Lung", "Spleen", "Muscle")
  
for (tissue in tissues) {
  current_file <- paste0("HK_", tolower(tissue), "_enhancer.xlsx")
  current_data <- read_excel(current_file)
    
  merged_data_HK <- rbind(merged_data_HK, current_data)
}
  
write.xlsx(merged_data_HK, "HK_enhancer_genes.xlsx",row.names = FALSE)
  
  
#Tissue specific enhancer target genes
# Read specific coordinates data for each tissue
sp_liver <- read_excel("target_gene_RE_count.xlsx", sheet = "Liver_specific_coordinates") #Liver_specific_coordinates
sp_lung <- read_excel("target_gene_RE_count.xlsx", sheet = "Lung_specific_coordinates") #Lung_specific_coordinates.txt
sp_spleen <- read_excel("target_gene_RE_count.xlsx", sheet = "Spleen_specific_coordinates") #Spleen_specific_coordinates
sp_muscle <- read_excel("target_gene_RE_count.xlsx", sheet = "Muscle_specific_coordinates") #Muscle_specific_coordinates
  
tissues <- c("Liver", "Lung", "Spleen", "Muscle")
for (tissue in tissues) {
  enhancer_data <- merged_vep_enhancer[merged_vep_enhancer$tissue == tissue, ]
    
  matching_genes <- enhancer_data[enhancer_data$gene %in% get(paste0("sp_", tolower(tissue)))$gene, "gene"]
    
  matching_info <- get(paste0("sp_", tolower(tissue)))[get(paste0("sp_", tolower(tissue)))$gene %in% matching_genes, c("gene", "biotype", "gene_name")]
    
  subset_enhancer <- enhancer_data[enhancer_data$gene %in% matching_genes, ]
  enhancer_sp <- merge(subset_enhancer, matching_info, by = "gene", all.x = TRUE)
    
  write.xlsx(enhancer_sp, paste0("SP_", tolower(tissue), "_enhancer.xlsx"),row.names = FALSE)
}
  
merged_data_SP <- data.frame()
  
tissues <- c("Liver", "Lung", "Spleen", "Muscle")
  
for (tissue in tissues) {
  current_file <- paste0("SP_", tolower(tissue), "_enhancer.xlsx")
  current_data <-read_excel(current_file)
    
  merged_data_SP <- rbind(merged_data_SP, current_data)
}
  
write.xlsx(merged_data_SP, "SP_enhancer_genes.xlsx", row.names = FALSE)

#Compare HK and  SP enhancer target genes
my_data_hk <- read_excel("HK_enhancer_genes.xlsx")
my_data_sp <- read_excel("SP_enhancer_genes.xlsx")

df_hk <- data.frame(my_data_hk)
df_sp <- data.frame(my_data_sp)

#SNP density in 1kb 
df_hk['SNP_1Kb'] <- df_hk$value*1000
df_sp['SNP_1Kb'] <- df_sp$value*1000


#Plot Without outliers
#For df_hk (housekeeping genes)
p<-ggplot(df_hk, aes(x = state, y = SNP_1Kb, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Enhancers of housekeeping") + 
  ylab("SNP density / 1kb")+
  ylim(0,25)+ 
  scale_x_discrete(limits = c("E6", "E7", "E8", "E9", "E10"), labels = c("EnhA", "EnhAMe", "EnhAWk", "EnhAHet", "EnhPois"))
png("hk_enhancer_density.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()


# For df_sp (specific genes)
p<-ggplot(df_sp, aes(x = state, y = SNP_1Kb, fill = tissue)) + 
  geom_boxplot(outlier.shape = NA) +
  xlab("Enhancers of Specific genes") + 
  ylab("SNP density / 1kb")+
  ylim(0,25) +
  scale_x_discrete(limits = c("E6", "E7", "E8", "E9", "E10"), labels = c("EnhA", "EnhAMe", "EnhAWk", "EnhAHet", "EnhPois"))
png("specific_enhancer_density.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

# Calculate mean of SNP_1Kb
#For df_hk (housekeeping genes)
df_hk_means <- df_hk %>%
  filter(tissue %in% c("Liver", "Lung", "Spleen", "Muscle")) %>%
  group_by(tissue, state) %>%
  summarise(mean_SNP_1Kb = mean(SNP_1Kb), .groups = 'drop')
df_hk_means
# For df_sp (specific genes)
df_sp_means <- df_sp %>%
  filter(tissue %in% c("Liver", "Lung", "Spleen", "Muscle")) %>%
  group_by(tissue, state) %>%
  summarise(mean_SNP_1Kb = mean(SNP_1Kb), .groups = 'drop')
df_sp_means

# Create the density plot for distance
# Combine data frames and add a 'source' column

df_hk$source <- "housekeeping"
mean(df_hk$dis)
df_sp$source <- "specific"
mean(df_sp$dis)

combined_df <- rbind(df_hk, df_sp)

# Create the density plot
p<-ggplot(combined_df, aes(x = dis, fill = source, color = source, linetype = source)) +
  geom_density(alpha = 0.4) +
  xlab("Distance") +
  ylab("Density") +
  theme_minimal() +
  labs(title = "Density plot of distances for housekeeping and specific genes")
png("hk_specific_distance_enhancer.png", width = 3627, height = 3627, res = 300)
plot(p)
dev.off()

#difference between variation in hk and sp
# Combine the mean SNP data
df_means <- merge(df_hk_means, df_sp_means, by = c("tissue", "state"), suffixes = c("_hk", "_sp"))

# Calculate the difference in mean SNP density
df_means$mean_diff = df_means$mean_SNP_1Kb_sp - df_means$mean_SNP_1Kb_hk

print(df_means)
#------------------------------------------------------------------------------

################################################### 
#Step 6: CAPE Analysis to Find Fragile Enhancers
################################################### 

#6A: Install the Program
# Clone the repository
cd /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE

git clone https://github.com/ncbi/dcode-cape
cd dcode-cape

# Load the required module
module load boost

# Build the project
make

cd ./bin
#Note: all programs should be in bin folder

#6B: Format the Reference Genome Using `formatFasta`

#Adjust the reference
#Input: Sus_scrofa.Sscrofa11.1.dna.toplevel.fa and Output: sus_new.fa
cp ../../trim_header.py ./
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/Sscrofa_genome/Sus_scrofa.Sscrofa11.1.dna.toplevel.fa ./
python trim_header.py

#Creating the binary format
#Input: sus_new.fa and Output: sus_new.fa.bin
./formatFasta -i sus_new.fa -o sus_new.fa.bin

# Generating the genome index 
samtools faidx sus_new.fa
cut -f1,2 sus_new.fa.fai > sus_new.genome
rm Sus_scrofa.Sscrofa11.1.dna.toplevel.fa

#6C: Generate K-mer Weights Using `kweight`
#Copy the files in bin folder
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Pig_Liver_E6 ./
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Pig_Liver_E7 ./
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Pig_Liver_E8 ./
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Pig_Liver_E9 ./
cp /lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Pig_Liver_E10 ./

#Formating the files: Remove chr
awk '{print substr($1,4),$2,$3}'  Pig_Liver_E6 >  Liver_E6.bed 
awk '{print substr($1,4),$2,$3}'  Pig_Liver_E7 >  Liver_E7.bed
awk '{print substr($1,4),$2,$3}'  Pig_Liver_E8 >  Liver_E8.bed
awk '{print substr($1,4),$2,$3}'  Pig_Liver_E9 >  Liver_E9.bed
awk '{print substr($1,4),$2,$3}'  Pig_Liver_E10 >  Liver_E10.bed

#Tab delimitted file
awk '{ $1=$1; print }' OFS="\t" Liver_E6.bed > Liver_E6_fixed.bed 
awk '{ $1=$1; print }' OFS="\t" Liver_E7.bed > Liver_E7_fixed.bed
awk '{ $1=$1; print }' OFS="\t" Liver_E8.bed > Liver_E8_fixed.bed
awk '{ $1=$1; print }' OFS="\t" Liver_E9.bed > Liver_E9_fixed.bed
awk '{ $1=$1; print }' OFS="\t" Liver_E10.bed > Liver_E10_fixed.bed

# Generate K-mer weights
./kweight -b  Liver_E6_fixed.bed -m sus_new.fa.bin -g -n 2 -p E6_kweight_out.txt
./kweight -b  Liver_E7_fixed.bed -m sus_new.fa.bin -g -n 2 -p E7_kweight_out.txt
./kweight -b  Liver_E8_fixed.bed -m sus_new.fa.bin -g -n 2 -p E8_kweight_out.txt
./kweight -b  Liver_E9_fixed.bed -m sus_new.fa.bin -g -n 2 -p E9_kweight_out.txt
./kweight -b  Liver_E10_fixed.bed -m sus_new.fa.bin -g -n 2 -p E10_kweight_out.txt
#-g  : Generate control from chromosomes.
#-n  : Number of controls per peak (default: 10, The options -g -n 3 was optimized for human samples. In pig,  with -n 2 we can get the controls ).

rm *.bed

#Generate zip files
gzip E*.txt

#6D: Merging kweight Files
# kweight_list contains the names of kweight files, each on a separate line:
#Generate kweight_list.txt
echo E6_kweight_out.txt.gz > kweight_list.txt
echo E7_kweight_out.txt.gz >> kweight_list.txt
echo E8_kweight_out.txt.gz >> kweight_list.txt
echo E9_kweight_out.txt.gz >> kweight_list.txt
echo E10_kweight_out.txt.gz >> kweight_list.txt

rm *_out.txt.gz

#Run kmerge
./kmerge -i kweight_list.txt -o kmerge_out.txt

#6E: Generating a SVM model using libSVM trainning code by snp2svmModel

# Create a file including <chromosom number> <snp id> <pos snp> <all> <alt> <1/-1> such as snp_test.txt
cp ../../snp_test.txt ./
#Create config file
echo in snp_test.txt > config_svm_tmp.txt
echo svm_type 0 >> config_svm_tmp.txt
echo kernel_type 0 >> config_svm_tmp.txt
echo w1 4 >> config_svm_tmp.txt
echo probability 1 >> config_svm_tmp.txt
echo model ./svm.model >> config_svm_tmp.txt
echo chrs ./sus_new.fa.bin >> config_svm_tmp.txt
echo weight ./kmerge_out.txt >> config_svm_tmp.txt
echo neighbors 100 >> config_svm_tmp.txt
echo fimo 0 >> config_svm_tmp.txt
#Tab delimited config file
awk '{$1=$1}1' OFS="\t" config_svm_tmp.txt > config_svm.txt
#Run the program
snp2svmModel -i config_svm.txt

#6F: Calculations using CAPE
#Copy a vcf file
cp  /lustre/backup/SHARED/TOPIGS_WUR/BAMS_11.1/freebayes_merged/chr1_merged.vep.vcf.gz ./
gunzip chr1_merged.vep.vcf.gz
#select the required columns (chr, pos, snp Id, ref, alt)
awk '{if(!/^#/){print $1 "\t" $2 "\t" $3 "\t" $4 "\t" $5}}' chr1_merged.vep.vcf > chr1_input_snp.txt
rm chr1_merged.vep.vcf
#generates the unique SNP IDs (snp<chr>_<positions>) 
awk 'BEGIN{OFS="\t"} {if($3 == ".") $3 = "snp"$1"_"$2; print $0}' chr1_input_snp.txt > input_snp.txt
rm chr1_input_snp.txt
#Generate the config file
echo in	./input_snp.txt > config_cape_tmp.txt
echo out	./output_file_name.out >> config_cape_tmp.txt
echo order	10 >> config_cape_tmp.txt
echo chrs	./sus_new.fa.bin >> config_cape_tmp.txt
echo weight	./kmerge_out.txt >> config_cape_tmp.txt
echo neighbors	100 >> config_cape_tmp.txt
echo model	./svm.model >> config_cape_tmp.txt
echo probability	1 >> config_cape_tmp.txt
echo fimo	0 >> config_cape_tmp.txt
#Tab delimited config file
awk '{$1=$1}1' OFS="\t" config_cape_tmp.txt > config_cape.txt
#Run the program
cape -i config_cape.txt