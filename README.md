# Revised README: PPS-LIFT Project Part-III

## Regulatory Element Analysis Pipeline

- **Author:** Samin HF
- **Date:** 27/02/2022

## Overview
The PPS-LIFT Project Part-III focuses on a pipeline designed to systematically analyze regulatory elements to identify variations. It integrates genomic and regulatory element data related to SNPs of pureline and the results of eGWAS used in the PPS-LIFT project, along with promoters and enhancers dataset extracted from the Pan et al. (2021) study.

The current directories and codes are tailored for the third part of the PPS-LIFT project, a collaboration between the Animal Breeding and Genomics (ABG) group at Wageningen University & Research and Topigs Norsvin.


## Setup
Before executing the pipeline, adjustments to `RegE_analysis.sh` are necessary to align with the specific directories and codes of your project. Once modified, execute the bash file as follows:

```bash
git clone git@gitlab.com:samin.h.farhangi/pps-lift-3.git
cd pps-lift-3
sbatch RegE_analysis.sh
```

## Pipeline Steps
1. **Genomic Data Preparation**
2. **Regulatory Element (RE) Data Preparation**
3. **Merging of RE and Genomic Data**
4. **Identification of Target Genes of Promoters**
5. **Identification of Target Genes of Enhancers**
6. **CAPE Analysis for Fragile Enhancers Identification**

### Step 1: Genomic Data Preparation
#### 1A: SNP Calling
Execute the `count_breed_num.sh` script:
```bash
breed=<PATH to breed line>
for b in `cat $breed`
	do
	echo $b
	sbatch count_breed_num.sh $b
done
```

#### 1B: Merging SNP Files
```bash
dir=<PATH to output of 1A>
paste $dir/* > $dir/Pig_breed_num_vep.txt
```
> Additional commands are available in `RegE_analysis.sh`.

### Step 2: RE Data Preparation
#### 2A: Download RE Data
Save the bed file of promoters and enhancers related to each tissue.

#### 2B: Counting Number of RE States per Tissue
```awk
awk '{count[$NF]++} END {for (state in count) print state, count[state]}' <bed file per tissue>
```

#### 2C: Separating RE Regions
```python
python RE_Pan.py <bed file per tissue>
```

#### 2D: Merging Promoter and Enhancer Files
```bash
cat *_promoter.txt > all_tissue_promoter.txt
cat *_enhancer.txt > all_tissue_enhancer.txt
```

#### 2E & 2F: Plotting and Coverage Analysis
Commands for plotting the length of RE for all states and per tissue, and for coverage of RE states per tissue over the genome are available in `RegE_analysis.sh`.

### Step 3: Merging RE Data and Genomic Data
#### 3A: SNP Density within RE Regions
```bash
bash count_length.sh
bash clean_up.sh
bash overlap_count.sh
```

#### 3B: SNP Density of Random Coordinates within RE Regions
```bash
bash random_extract.sh
bash random_variants.sh
```

#### 3C - 3D: Statistical Analysis
Commands for statistical analysis and Wilcoxon test are available in `RegE_analysis.sh`.

#### 3E: eGWAS-SNP Density within RE Regions
```bash
bash RE_seg_per_tissue.sh
Commands for analysis per tissue are available in `RegE_analysis.sh`.
```

#### 3F-3G: Statistical Analysis
Commands for generating the random file and one-sample t-test are available in `RegE_analysis.sh`.


### Step 4: Target Genes of Promoters
#### 4A - 4E: Extraction and Annotation
Commands for extracting and annotating tissue-specific genes, non-tissue-specific genes, housekeeping genes, and calculating the distance of promoters from genes are available in `RegE_analysis.sh`.

### Step 5: Target Genes of Enhancers
#### 5A & 5B: Extraction of Target Genes
Commands for extracting the target genes based on `H3K27ac_CTCFTAD` files and extraction of tissue-specific and HK target genes are available in `RegE_analysis.sh`.

### Step 6: CAPE Analysis to Find Fragile Enhancers
#### Introduction:
CAPE (Computational Analysis of gene Perturbation) assesses the probability of a genetic variant disrupting enhancer activity by altering the binding affinity of transcription factors (TFs) within a specific cellular context. It utilizes a k-mer based Support Vector Machine (SVM) model to identify harmful regulatory SNPs affecting gene expression and chromatin accessibility.

#### 6A - 6F: Installation and Calculations
Commands for installation, formatting the reference, generating k-mer weights, merging kweight files, generating SVM model, and calculations using CAPE are provided in the respective subsections.

> Note: All programs are located in the 'bin' folder.

---
