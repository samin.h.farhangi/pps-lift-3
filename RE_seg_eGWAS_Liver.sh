module load bedtools/gcc/64/2.28.0
enh=/lustre/nobackup/WUR/ABGC/shared/Pig/RegE/egwas_snp/
dir_eGWAS_liver=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp/1.Liver_seg
geno=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp/
DATA=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp/prom_enh_states.txt

for RE in `cat $DATA`
do
 echo $RE
        bedtools intersect -a $dir_eGWAS_liver/filtered_Liver_signBH_genes.bed -b $enh/Pig_${RE}_Liver_vep_count.txt -bed -wa -wb | wc -l > $dir_eGWAS_liver/Number_${RE}_found_shared.txt
        r=$(wc -l $dir_eGWAS_liver/filtered_Liver_signBH_genes.bed)
        for i in {1..100}; do shuf -n"$r" $geno/QC_100samples_filt2.bim | awk '{print $1"\t"$4"\t"$4}' > $dir_eGWAS_liver/temp_${i}filtered_Liver_signBH_genes.bed; done
        for i in {1..100}; do bedtools intersect -a $dir_eGWAS_liver/temp_${i}filtered_Liver_signBH_genes.bed -b $enh/Pig_${RE}_Liver_vep_count.txt -bed -wa -wb | wc -l >> $dir_eGWAS_liver/${RE}_overlap.txt; done
        rm $dir_eGWAS_liver/temp_*filtered_Liver_signBH_genes.bed
done
