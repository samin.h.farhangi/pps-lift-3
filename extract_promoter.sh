conda create -n bedops -c bioconda bedops
conda install -c bioconda bedops
source "$(conda info --base)/etc/profile.d/conda.sh"
conda activate bedops

dir_quant=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/
enh=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/egwas_snp/
tissue=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/list_tissues.txt

for tissue in `cat $tissue`
do
	echo $tissue
	#Housekeeping genes
	#E1
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/wei_housekeeping_protein_coding_fwd.txt $enh/Pig_E1_${tissue}_vep_count.txt | awk '{print $7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$5}' > $dir_quant/E1_genes_housekeeping_${tissue}_fwd.txt
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/wei_housekeeping_protein_coding_rev.txt $enh/Pig_E1_${tissue}_vep_count.txt | awk '{print $14"\t"$15"\t"$16"\t"$17"\t"$18"\t"$19"\t"$20"\t"$5}' > $dir_quant/E1_genes_housekeeping_${tissue}_rev.txt
	#E2
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/wei_housekeeping_protein_coding_fwd.txt $enh/Pig_E2_${tissue}_vep_count.txt | awk '{print $7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$5}' > $dir_quant/E2_genes_housekeeping_${tissue}_fwd.txt
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/wei_housekeeping_protein_coding_rev.txt $enh/Pig_E2_${tissue}_vep_count.txt | awk '{print $14"\t"$15"\t"$16"\t"$17"\t"$18"\t"$19"\t"$20"\t"$5}' > $dir_quant/E2_genes_housekeeping_${tissue}_rev.txt
	#E12
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/wei_housekeeping_protein_coding_fwd.txt $enh/Pig_E12_${tissue}_vep_count.txt | awk '{print $7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$5}' > $dir_quant/E12_genes_housekeeping_${tissue}_fwd.txt
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/wei_housekeeping_protein_coding_rev.txt $enh/Pig_E12_${tissue}_vep_count.txt | awk '{print $14"\t"$15"\t"$16"\t"$17"\t"$18"\t"$19"\t"$20"\t"$5}' > $dir_quant/E12_genes_housekeeping_${tissue}_rev.txt
			
	#C.2 Tissue-specific genes
	#E1
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/${tissue}_specific_coordinates_sorted_fwd.txt $enh/Pig_E1_${tissue}_vep_count.txt | awk '{print $7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$5}' > $dir_quant/E1_genespecific_${tissue}_fwd.txt
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/${tissue}_specific_coordinates_sorted_rev.txt $enh/Pig_E1_${tissue}_vep_count.txt | awk '{print $14"\t"$15"\t"$16"\t"$17"\t"$18"\t"$19"\t"$20"\t"$5}' > $dir_quant/E1_genespecific_${tissue}_rev.txt

	#E2
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/${tissue}_specific_coordinates_sorted_fwd.txt $enh/Pig_E2_${tissue}_vep_count.txt | awk '{print $7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$5}' > $dir_quant/E2_genespecific_${tissue}_fwd.txt
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/${tissue}_specific_coordinates_sorted_rev.txt $enh/Pig_E2_${tissue}_vep_count.txt | awk '{print $14"\t"$15"\t"$16"\t"$17"\t"$18"\t"$19"\t"$20"\t"$5}' > $dir_quant/E2_genespecific_${tissue}_rev.txt

	#E12
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/${tissue}_specific_coordinates_sorted_fwd.txt $enh/Pig_E12_${tissue}_vep_count.txt | awk '{print $7"\t"$8"\t"$9"\t"$10"\t"$11"\t"$12"\t"$13"\t"$5}' > $dir_quant/E12_genespecific_${tissue}_fwd.txt
	closest-features --dist --no-overlaps --delim '\t' $dir_quant/${tissue}_specific_coordinates_sorted_rev.txt $enh/Pig_E12_${tissue}_vep_count.txt | awk '{print $14"\t"$15"\t"$16"\t"$17"\t"$18"\t"$19"\t"$20"\t"$5}' > $dir_quant/E12_genespecific_${tissue}_rev.txt
	
	#Merge both files
	cat $dir_quant/E1_genes_housekeeping_${tissue}_fwd.txt  $dir_quant/E1_genes_housekeeping_${tissue}_rev.txt | grep -v "NA" | awk '$2!=""' > $dir_quant/E1_genes_housekeeping_${tissue}_merged.txt
	cat $dir_quant/E1_genespecific_${tissue}_fwd.txt $dir_quant/E1_genespecific_${tissue}_rev.txt | grep -v "NA" | awk '$2!=""' > $dir_quant/E1_genespecific_${tissue}_merged.txt	

	cat $dir_quant/E2_genes_housekeeping_${tissue}_fwd.txt  $dir_quant/E2_genes_housekeeping_${tissue}_rev.txt | grep -v "NA" | awk '$2!=""' > $dir_quant/E2_genes_housekeeping_${tissue}_merged.txt
	cat $dir_quant/E2_genespecific_${tissue}_fwd.txt $dir_quant/E2_genespecific_${tissue}_rev.txt | grep -v "NA" | awk '$2!=""' > $dir_quant/E2_genespecific_${tissue}_merged.txt	

	cat $dir_quant/E12_genes_housekeeping_${tissue}_fwd.txt  $dir_quant/E12_genes_housekeeping_${tissue}_rev.txt | grep -v "NA" | awk '$2!=""' > $dir_quant/E12_genes_housekeeping_${tissue}_merged.txt
	cat $dir_quant/E12_genespecific_${tissue}_fwd.txt $dir_quant/E12_genespecific_${tissue}_rev.txt | grep -v "NA" | awk '$2!=""' > $dir_quant/E12_genespecific_${tissue}_merged.txt	

	
done
conda deactivate
