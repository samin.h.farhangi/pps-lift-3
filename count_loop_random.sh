#!/bin/bash
#SBATCH --time=1-25:00:00
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=4000
#SBATCH --qos=Std
#SBATCH --output=slurm.output_%j.txt
module load SHARED/bedtools/2.18.0
dir_genome=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/Sscrofa_genome/
enh=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
i=$1
echo $i
echo "starting analysis"
bedtools shuffle -noOverlapping -i ${enh}/Pig_${i}_length_up.bed -g ${dir_genome}/chrNameLength_noScaffold.txt > ${enh}/Pig_${i}_length_up_random_temp.bed 
echo $i
echo "done!"
