enh=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
t_seg=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Tissue_segment.txt
for i in `cat $t_seg`
do
	echo $i >> $enh/Summary_statistics_num_variants.txt
	wc -l < Pig_${i}_vep_count_temp.txt >> $enh/Summary_statistics_num_variants.txt
	awk '{print $8}' Pig_${i}_vep_count_temp.txt | awk -f calculate_mean.awk >> $enh/Summary_statistics_num_variants.txt
	echo $i" random" >> $enh/Summary_statistics_num_variants.txt
	wc -l < Pig_${i}_vep_count_random_temp.txt >> $enh/Summary_statistics_num_variants.txt
	awk '{print $8}' Pig_${i}_vep_count_random_temp.txt | awk -f calculate_mean.awk >> $enh/Summary_statistics_num_variants.txt
done