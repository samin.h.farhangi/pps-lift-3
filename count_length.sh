seg=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/segments.txt  #E1:E15
for i in `cat $seg`
do
 echo $i
	awk '{print $0"\t"$3-$2"\t""Liver"}' Pig_Liver_${i} > Pig_Liver_${i}_length
	awk '{print $0"\t"$3-$2"\t""Spleen"}' Pig_Spleen_${i} > Pig_Spleen_${i}_length
	awk '{print $0"\t"$3-$2"\t""Lung"}' Pig_Lung_${i} > Pig_Lung_${i}_length
	awk '{print $0"\t"$3-$2"\t""Muscle"}' Pig_Muscle_${i} > Pig_Muscle_${i}_length
done