#!/bin/bash
#SBATCH --time=1-20:00:00
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=4000
#SBATCH --qos=Std
#SBATCH --output=slurm.output_%j.txt
module load bedtools/gcc/64/2.28.0
module load bcftools/gcc/64/1.9
chr=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/chr.txt
dir=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
dir_vcf=/lustre/backup/SHARED/TOPIGS_WUR/BAMS_11.1/freebayes_merged
i=$1
for j in `cat $chr`
	do
	echo $j
	bcftools view -i 'AF>0.0' $dir_vcf/chr${j}_merged.vep.vcf.gz  | bcftools view -i 'MAF>0.1' | bedtools intersect -a stdin -b $dir/Pig_${i}_length_up.bed -wb -wa | awk -F'\t' '{print $(NF-5)"\t"$(NF-4)"\t"$(NF-3)"\t"$(NF-2)"\t"$(NF-1)"\t"$NF}' | uniq -c | awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$7"\t"$6"\t"$1/$6}' | uniq >> $dir/Pig_${i}_vep_count_temp.txt
done