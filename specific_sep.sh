tissue=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/list_tissues.txt
dir_quant=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/
for tissue in `cat $tissue`
do
 echo $tissue
	awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' $dir_quant/${tissue}_specific_coordinates.txt | sort -k 1,1 -k2,2n | grep "protein_coding" > $dir_quant/${tissue}_specific_coordinates_sorted.txt
	awk '($4=="+")' $dir_quant/${tissue}_specific_coordinates_sorted.txt > $dir_quant/${tissue}_specific_coordinates_sorted_fwd.txt
	awk '($4=="-")' $dir_quant/${tissue}_specific_coordinates_sorted.txt > $dir_quant/${tissue}_specific_coordinates_sorted_rev.txt
done
