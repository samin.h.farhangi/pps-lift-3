#!/bin/bash
#SBATCH --time=25:00:00
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=4000
#SBATCH --qos=Std
#SBATCH --output=slurm.output_%j.txt
module load bedtools/gcc/64/2.28.0
module load bcftools/gcc/64/1.9
chr=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/chr.txt #1-18, X
dir=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/
dir_vcf=/lustre/backup/SHARED/TOPIGS_WUR/BAMS_11.1/freebayes_merged
dir_breed=/lustre/backup/SHARED/TOPIGS_WUR/BAMS_11.1/freebayes_merged/Breed_VCFs  #SNP calling based on the refseq
b=$1
for j in `cat $chr`
	do
	echo $j
    bcftools view --force-samples -S $dir_breed/${b}.tsv $dir_vcf/chr${j}_merged.vep.vcf.gz |  bcftools +/cm/shared/apps/bcftools/gcc/64/1.9/libexec/bcftools/fill-tags.so | bcftools view -i 'AF>0.0' | bcftools view -i 'MAF>0.1' | wc -l >> $dir/Pig_${b}_num_vep.txt
done
echo "done"
