#!/usr/bin/env python

"""
@author: hosse004; 2-27-2022

This script processes the result of CPM counting (Final_counts_CPM.txt) and separates the CPM of each tissue.
Input: Final_counts_CPM.txt
Output: A_counts_CPM.txt; B_counts_CPM.txt; C_counts_CPM.txt; D_counts_CPM.txt
"""

from sys import argv

def parse_cpm_file(cpm_file):
    """
    Parses the CPM file and separates the CPM values for each tissue.
    
    Args:
    - cpm_file: File object for the CPM file.
    
    Returns:
    - A_list, B_list, C_list, D_list: Lists containing CPM values for each tissue.
    - names: List of dataset names.
    - ID_list: List of gene IDs.
    """
  
    ID_list = []  # ID_number of each gene which are in the first column
    names = []    # names of each dataset which are in the first row
    A_list = []   # cpm of A tissues 
    B_list = []   # cpm of B tissues 
    C_list = []   # cpm of C tissues
    D_list = []   # cpm of D tissues 

    for line in cpm_file:
        cpm_file_line = line.strip().split()

        if line.startswith('WUR'): 
            names = cpm_file_line[::4]  # Four names have one dataset number
        else:
            Gene_ID = cpm_file_line[0]
            ID_list.append(Gene_ID)
            A_list.append(cpm_file_line[1::4])
            B_list.append(cpm_file_line[2::4])
            C_list.append(cpm_file_line[3::4])
            D_list.append(cpm_file_line[4::4])

    return A_list, B_list, C_list, D_list, names, ID_list  


def write_output(cpm_list, names, ID_list, outname):
    """
    Writes the output file.
    
    Args:
    - cpm_list: List of CPM values.
    - names: List of dataset names.
    - ID_list: List of gene IDs.
    - outname: Name of the output file.
    
    Returns:
    - None
    """
    with open(outname, 'w') as out:
        # Writing the name of datasets excluding '_A'/'_B'/'_C'/'_D' in the first row
        for name in names:
            out.write('\t%s\t' % (name[:-2]))
        out.write("\n")

        for i in range(len(ID_list)):
            ID = ID_list[i]  # ID name of genes
            cpm = cpm_list[i]  # cpm of each list

            out.write('%s\t' % (ID))
            for j in range(len(cpm)):
                out.write('\t%s' % (cpm[j]))
            out.write("\n")


if __name__ == "__main__":
    infile_name = 'Final_counts_CPM.txt'
    with open(infile_name, 'r') as infile:
        A_list, B_list, C_list, D_list, names, ID_list = parse_cpm_file(infile)

    outname_A = 'A_counts_CPM.txt'
    outname_B = 'B_counts_CPM.txt'
    outname_C = 'C_counts_CPM.txt'
    outname_D = 'D_counts_CPM.txt'

    write_output(A_list, names, ID_list, outname_A)
    write_output(B_list, names, ID_list, outname_B)
    write_output(C_list, names, ID_list, outname_C)
    write_output(D_list, names, ID_list, outname_D)
