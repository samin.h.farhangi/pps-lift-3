dir_quant=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/
dir_genome=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/6.GenotypeQC/1.eGWAS/eGWAS_filter/
DATA=/lustre/nobackup/WUR/ABGC/shared/Pig/PPS_LIFT/RegE/Promoter_target_genes/faang_tissues.txt
for tissue in `cat $DATA`
do
 echo $tissue
	awk 'NR==FNR{a[$1]=$0} NR>FNR && a[$5]>0{print $0}' $dir_quant/${tissue}_specific.txt $dir_genome/Sus_scrofa.Sscrofa11.1.104.bed > $dir_quant/${tissue}_specific_coordinates.txt
	wc -l $dir_quant/${tissue}_specific_coordinates.txt
done
